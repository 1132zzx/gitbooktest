# bug in gitlab-page deploy

.gitlab-ci.yml
```yml
image: node:4.2.2
# note  GitBook 2.6.9 failed

cache:
  paths: 
    - node_modules/ 

before_script:
  - npm install gitbook-cli -g
  - gitbook fetch 3.2.3
  - gitbook install
  
pages:
  stage: deploy
  script:
    - gitbook build
  artifacts:
    paths:
      - public 
  only:
    - master # this job will affect only the 'master' branch
```

```
Error loading version latest: SyntaxError: Unexpected token {
     at exports.runInThisContext (vm.js:53:16)
     at Module._compile (module.js:414:25)
     at Object.Module._extensions..js (module.js:442:10)
     at Module.load (module.js:356:32)
     at Function.Module._load (module.js:311:12)
     at Module.require (module.js:366:17)
     at require (module.js:385:17)
     at Object.<anonymous> (/root/.gitbook/versions/3.2.3/node_modules/read-installed/node_modules/read-package-json/read-json.js:14:27)
     at Module._compile (module.js:435:26)
     at Object.Module._extensions..js (module.js:442:10)
 TypeError: Cannot read property 'commands' of null
```


cache.zip : https://storage.googleapis.com/gitlab-com-runners-cache/project/18499735/default

for audit need to run : ``npm shrinkwrap`` to get the lock file
